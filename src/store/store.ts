import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { chatReducer } from "./chat-slice";
import { getDefaultMiddleware } from "@reduxjs/toolkit";

const customizedMiddleware = getDefaultMiddleware({
  serializableCheck: false,
});

export const rootReducer = combineReducers({
  chat: chatReducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: customizedMiddleware,
});

export type RootState = ReturnType<typeof store.getState>;
