import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IMessage } from "../types";
import { RootState } from "./store";
import { v4 as uuidv4 } from "uuid";

export interface IChatState {
  messages: IMessage[];
  editModal: boolean;
  editMessageId?: string;
  editMessageText: string;
  preloader: boolean;
  inputText: string;
}

const initialState: IChatState = {
  messages: [],
  editModal: false,
  editMessageText: "",
  preloader: true,
  inputText: "",
};

export const chatSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {
    setPreloader: (state, action: PayloadAction<boolean>) => {
      state.preloader = action.payload;
    },
    setMessages: (state, action: PayloadAction<IMessage[]>) => {
      state.messages = action.payload;
    },
    setInputText: (state, action: PayloadAction<string>) => {
      state.inputText = action.payload;
    },
    sendMessage: state => {
      const message: IMessage = {
        id: uuidv4(),
        userId: "12245",
        text: state.inputText,
        ownMessage: true,
        isLiked: false,
        createdAt: new Date(),
        avatar: "",
        username: "Maxim",
      };

      state.messages = [...state.messages, message];
      state.inputText = "";
    },
    likeMessage: (state, action: PayloadAction<{ id: string }>) => {
      const copiedMessages = [...state.messages];
      const messageId = action.payload.id;
      const message = copiedMessages.find(({ id, ownMessage }) => id === messageId && !ownMessage);
      if (!message) return;

      message.isLiked = !message.isLiked;
      state.messages = copiedMessages;
    },
    editMessage: (state, action: PayloadAction<{ id: string }>) => {
      const {
        payload: { id },
      } = action;
      state.editModal = true;
      state.editMessageId = id;
      state.editMessageText = state.messages.find(message => message.id === id)?.text || "";
    },
    editLastOwnMessage: state => {
      const { messages } = state;
      const lastMessage = [...messages].reverse().find(message => message.ownMessage);
      if (!lastMessage) return;

      state.editModal = true;
      state.editMessageId = lastMessage.id;
      state.editMessageText = lastMessage.text;
    },
    updateMessage: (state, action: PayloadAction<string>) => {
      const { messages, editMessageId } = state;
      const { payload } = action;
      const message = messages.find(({ id }) => id === editMessageId);
      if (!message) {
        state.editModal = false;
        delete state.editMessageId;
        return;
      }

      message.text = payload;
      message.editedAt = new Date();
      state.messages = [...messages];
      state.editModal = false;
      delete state.editMessageId;
    },
    cancelEdit: state => {
      state.editModal = false;
      delete state.editMessageId;
    },
    deleteMessage: (state, action: PayloadAction<{ id: string }>) => {
      state.messages = [...state.messages].filter(message => message.id !== action.payload.id);
    },
  },
});

export const {
  setPreloader,
  setMessages,
  setInputText,
  sendMessage,
  likeMessage,
  editMessage,
  editLastOwnMessage,
  updateMessage,
  cancelEdit,
  deleteMessage,
} = chatSlice.actions;

const selectChat = (state: RootState) => state.chat;
export const selectPreloader = createSelector(selectChat, chat => chat.preloader);
export const selectMessages = createSelector(selectChat, chat => chat.messages);
export const selectMessagesCount = createSelector(selectChat, chat => chat.messages.length);
export const selectLastMessageAt = createSelector(selectChat, ({ messages }) => {
  if (messages.length) return messages[messages.length - 1].createdAt;
  else return new Date();
});
export const selectParticipantsCount = createSelector(selectChat, chat => {
  const userIds = chat.messages.map(({ userId }) => userId);
  return new Set(userIds).size;
});
export const selectInputText = createSelector(selectChat, chat => chat.inputText);
export const selectEditModal = createSelector(selectChat, chat => chat.editModal);
export const selectEditMessageText = createSelector(selectChat, chat => chat.editMessageText);

export const chatReducer = chatSlice.reducer;
