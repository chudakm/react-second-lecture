import React from "react";
import { useEffect } from "react";
import { MessageApi } from "../../api";
import { MessageMapper } from "../../mapper/message-mapper";
import "./chat.scss";
import { Header, MessageList, MessageInput } from "./components";
import { Preloader } from "./components/preloader";
import { editLastOwnMessage, selectPreloader, setMessages, setPreloader } from "../../store/chat-slice";
import { useDispatch, useSelector } from "react-redux";
import { EditModal } from "./components/edit-modal";

interface IChatProps {
  url: string;
}

export const Chat: React.FC<IChatProps> = ({ url }) => {
  const dispatch = useDispatch();
  const preloader = useSelector(selectPreloader);

  useEffect(() => {
    MessageApi.get(url).then(apiMessages => {
      const messages = MessageMapper.fromApiMessages(apiMessages);
      dispatch(setMessages(messages));
      dispatch(setPreloader(false));
    });
  }, [url, dispatch]);

  useEffect(() => {
    const onKeyDown = (e: KeyboardEvent) => {
      if (e.key === "ArrowUp") dispatch(editLastOwnMessage());
    };

    document.addEventListener("keydown", onKeyDown);

    return () => {
      document.removeEventListener("keydown", onKeyDown);
    };
  }, [dispatch]);

  if (preloader) {
    return <Preloader />;
  }

  return (
    <div className='chat'>
      <EditModal />

      <Header />
      <br />
      <MessageList />
      <br />
      <MessageInput />
    </div>
  );
};
