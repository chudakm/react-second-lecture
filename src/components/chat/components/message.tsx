import React from "react";
import classNames from "classnames";
import "../chat.scss";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { dateToHoursAndMinutes } from "../../../util/date-to-hours-and-minutes";
import { useDispatch } from "react-redux";
import { likeMessage } from "../../../store/chat-slice";

interface IMessageProps {
  id: string;
  text: string;
  createdAt: Date;
  isLiked: boolean;
  username: string;
  avatar: string;
}

export const Message: React.FC<IMessageProps> = ({ id, text, createdAt, isLiked, username, avatar }) => {
  const dispatch = useDispatch();

  const onLike = () => dispatch(likeMessage({ id }));

  return (
    <div className='message'>
      <div className='message-left'>
        <div className='message-user-avatar'>
          <img src={avatar} alt='avatar' />
        </div>
      </div>

      <div className='message-right'>
        <span className='message-user-name'>{username}</span>
        <span className='message-time'>{dateToHoursAndMinutes(createdAt)}</span>
        <FontAwesomeIcon
          className={classNames("like", { "message-like": !isLiked, "message-liked": isLiked })}
          onClick={onLike}
          icon={faHeart}
        />

        <br />

        <span className='message-text'>{text}</span>
      </div>
    </div>
  );
};
