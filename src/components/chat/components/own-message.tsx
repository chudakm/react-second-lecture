import React from "react";
import "../chat.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faTrash } from "@fortawesome/free-solid-svg-icons";
import { dateToHoursAndMinutes } from "../../../util/date-to-hours-and-minutes";
import { useDispatch } from "react-redux";
import { deleteMessage, editMessage } from "../../../store/chat-slice";

interface IOwnMessageProps {
  id: string;
  createdAt: Date;
  text: string;
}

export const OwnMessage: React.FC<IOwnMessageProps> = ({ id, createdAt, text }) => {
  const dispatch = useDispatch();

  const onEdit = () => {
    dispatch(editMessage({ id }));
  };

  const onDelete = () => {
    dispatch(deleteMessage({ id }));
  };

  return (
    <div className='own-message'>
      <span className='message-time'>{dateToHoursAndMinutes(createdAt)}</span>
      <FontAwesomeIcon className='message-edit' icon={faCog} onClick={onEdit}></FontAwesomeIcon>
      <FontAwesomeIcon className='message-delete' icon={faTrash} onClick={onDelete}></FontAwesomeIcon>
      <br />

      <span className='message-text'>{text}</span>
    </div>
  );
};
