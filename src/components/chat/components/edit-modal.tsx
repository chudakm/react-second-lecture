import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { cancelEdit, selectEditMessageText, selectEditModal, updateMessage } from "../../../store/chat-slice";
import "../chat.scss";

export const EditModal: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const editModal = useSelector(selectEditModal);
  const editMessageText = useSelector(selectEditMessageText);
  const [text, setText] = useState("");

  useEffect(() => {
    setText(editMessageText);
  }, [editMessageText]);

  const onMessageInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value);
  };

  const onSave = () => {
    dispatch(updateMessage(text));
    setText("");
  };

  const onCancel = () => {
    dispatch(cancelEdit());
    setText("");
  };

  return (
    <div className={`edit-message-modal ${editModal ? "modal-shown" : ""}`} role='dialog'>
      <span className='title'>Edit message</span>

      <textarea className='edit-message-input' cols={30} rows={10} value={text} onInput={onMessageInput}></textarea>
      <br />

      <div className='controls'>
        <button className='edit-message-button' onClick={onSave}>
          OK
        </button>
        <button className='edit-message-close' onClick={onCancel}>
          CANCEL
        </button>
      </div>
    </div>
  );
};
