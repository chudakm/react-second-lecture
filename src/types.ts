export interface IMessage {
  id: string;
  userId: string;
  avatar: string;
  username: string;
  text: string;
  createdAt: Date;
  editedAt?: Date;
  isLiked: boolean;
  ownMessage: boolean;
}

export interface IGroupedMessages {
  [group: string]: IMessage[];
}
